# Construct 3 Dec 2019-TouchMeAnimal.Full Version


Children's game, helps to learn numbers, develops concentration, memory and logic, improves cognitive skills.


Implemented menu navigation
Partical effect added
Music plays in the background during the game
When you press a digit on, a sound is played
There are buttons to return to the menu, turn off the sound

Added banner advertising and interstitial (google admob)

![alt text](https://gitlab.com/cartban1987/construct-3-dec-2019-touchmeanimal.full-version/-/raw/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA1.PNG)
